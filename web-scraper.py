#!/usr/bin/env python3

from lxml import html
import requests

page = requests.get('http://www.unkno.com/')
tree = html.fromstring(page.content)

fact = tree.xpath('//div[@id="content"]/text()')

trimmed_one = str(fact)[12:]
trimmed_two = trimmed_one[:-4]

print(trimmed_two)